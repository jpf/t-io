package org.tio.utils.hutool;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileUtil {

	/** 类Unix路径分隔符 */
	private static final char UNIX_SEPARATOR = '/';
	/** Windows路径分隔符 */
	private static final char WINDOWS_SEPARATOR = '\\';

	/**
	 * 获取文件扩展名，扩展名不带“.”
	 * 
	 * @param file 文件
	 * @return 扩展名
	 */
	public static String extName(File file) {
		if (null == file) {
			return null;
		}
		if (file.isDirectory()) {
			return null;
		}
		return extName(file.getName());
	}

	/**
	 * 获得文件的扩展名，扩展名不带“.”
	 * 
	 * @param fileName 文件名
	 * @return 扩展名
	 */
	public static String extName(String fileName) {
		if (fileName == null) {
			return null;
		}
		int index = fileName.lastIndexOf(".");
		if (index == -1) {
			return StrUtil.EMPTY;
		} else {
			String ext = fileName.substring(index + 1);
			// 扩展名中不能包含路径相关的符号
			return (ext.contains(String.valueOf(UNIX_SEPARATOR)) || ext.contains(String.valueOf(WINDOWS_SEPARATOR))) ? StrUtil.EMPTY : ext;
		}
	}

	/**
	 * @param data
	 * @param file
	 * @author tanyaowu
	 * @throws IOException 
	 */
	public static void writeBytes(byte[] data, File file) throws IOException {
		if (!file.exists()) {
			file.createNewFile();
		}

		//获取全路径
		String canonicalPath = file.getCanonicalPath();
		//通过Files获取文件的输出流
		OutputStream fos = null;
		try {
			fos = Files.newOutputStream(Paths.get(canonicalPath));
			fos.write(data);
			fos.flush();
		} finally {
			if (fos != null) {
				fos.close();
			}
			fos.close();
		}
	}

	/**
	 * 
	 * @param content
	 * @param path
	 * @param charset
	 * @author tanyaowu
	 * @throws IOException 
	 */
	public static void writeString(String content, String path, String charset) throws IOException {
		byte[] data = content.getBytes(charset);
		File file = new File(path);
		writeBytes(data, file);
	}
}
