package org.tio.utils.hutool;

import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author tanyaowu 
 * 2018年8月5日 下午8:43:31
 */
public class Validator {
	private static Logger log = LoggerFactory.getLogger(Validator.class);

	/**
	 * 
	 * @author tanyaowu
	 */
	public Validator() {
	}
	
	/**
	 * 通过正则表达式验证
	 * 
	 * @param pattern 正则模式
	 * @param value 值
	 * @return 是否匹配正则
	 */
	public static boolean isMactchRegex(Pattern pattern, String value) {
		return ReUtil.isMatch(pattern, value);
	}
	
	/**
	 * 验证是否为IPV4地址
	 * 
	 * @param value 值
	 * @return 是否为IPV4地址
	 */
	public static boolean isIpv4(String value) {
		return isMactchRegex(org.tio.utils.hutool.PatternPool.IPV4, value);
	}
}
